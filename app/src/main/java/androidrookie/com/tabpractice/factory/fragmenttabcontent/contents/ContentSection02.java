package androidrookie.com.tabpractice.factory.fragmenttabcontent.contents;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidrookie.com.tabpractice.R;

/**
 * This class is for tab test content
 * Created by potingchiang on 2017-04-04.
 */

public class ContentSection02 extends Fragment {

    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
//    private static final String ARG_SECTION_NUMBER  = "section_number";
//    private static final String ARG_RESOURCE_ID     = "resource_id";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
//    public static ContentTest newInstance(int sectionNumber) {
//        ContentTest fragment = new ContentTest();
//        Bundle args = new Bundle();
//        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
////        args.putInt(ARG_RESOURCE_ID, resourceId);
//        fragment.setArguments(args);
//        return fragment;
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_section02, container, false);
        return rootView;
    }
}
