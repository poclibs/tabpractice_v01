package androidrookie.com.tabpractice.factory.pageradaptertab.pagers;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import androidrookie.com.tabpractice.factory.fragmenttabcontent.contents.ContentSection01;
import androidrookie.com.tabpractice.factory.fragmenttabcontent.contents.ContentSection02;

/**
 * This is for tab pager adapter
 * Created by potingchiang on 2017-04-04.
 */

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private final int tabCount;

    public SectionsPagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {

        // getItem is called to instantiate the fragment for the given page.
        // Return a tab content (defined as a static class below).
       switch (position) {

           case 0: {

//               return new ContentTest();
               return new ContentSection01();
           }
           case 1: {

               return new ContentSection02();
           }
//           case 2: {
//
//                return new ContentSection02();
//           }
//           case 3: {
//
//               return new ContentTest();
//           }

        }

        return null;
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {

            case 0: {

                return "Summary";
            }

            case 1: {

                return "Management";
            }
        }
        return "Error";
    }

}
